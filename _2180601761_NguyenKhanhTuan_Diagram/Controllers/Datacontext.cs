﻿using _2180601761_NguyenKhanhTuan_Diagram.Models;
using Microsoft.EntityFrameworkCore;

namespace _2180601761_NguyenKhanhTuan_Diagram.Controllers
{
    public class Datacontext : DbContext
    {
        public Datacontext(DbContextOptions<Datacontext> options) : base(options)
        {
        }
        public DbSet<Customer > Customers { get; set; }
        public DbSet<Transactions> Transactions { get; set; }
        public DbSet<Emloyees> Emloyees { get; set; }
        public DbSet<Logs> Logs { get; set; }
        public DbSet<Reports> Reports { get; set; }
        public DbSet<Accounts> Accounts { get; set; }
    }
}
