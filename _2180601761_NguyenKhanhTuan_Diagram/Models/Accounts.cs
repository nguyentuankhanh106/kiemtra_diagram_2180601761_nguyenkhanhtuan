﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _2180601761_NguyenKhanhTuan_Diagram.Models
{
    public class Accounts
    {
       
         public int Id { get; set; }
        [Required, StringLength(50)]
        public string AccountName { get; set; }
        public int CustomerId { get; set; }
        public Customer? Custommer { get; set; }
        public List<Reports> Reports { get; set; }
      
    }
}
