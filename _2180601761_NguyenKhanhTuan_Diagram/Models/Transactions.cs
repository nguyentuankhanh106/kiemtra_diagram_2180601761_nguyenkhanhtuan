﻿using System.ComponentModel.DataAnnotations;

namespace _2180601761_NguyenKhanhTuan_Diagram.Models
{
    public class Transactions
    {

        public int Id { get; set; }
        [Required, StringLength(50)]
        public string Name { get; set; }

        public int CustomerId { get; set; }
        public Customer? Custommer { get; set; }

        public int EmployeeId { get; set; }
        public Emloyees? Emloyees { get; set; }


        public List<Reports> Reports { get; set; }
        public List<Logs> Logs { get; set; }
    }
}
