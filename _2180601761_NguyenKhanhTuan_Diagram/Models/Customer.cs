﻿using System.ComponentModel.DataAnnotations;

namespace _2180601761_NguyenKhanhTuan_Diagram.Models
{
    public class Customer
    {

        public int Id { get; set; }
        [Required, StringLength(50)]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactAndAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public List<Accounts> Accounts { get; set; }
        public List<Transactions> Transactions { get; set; }
    }
}
