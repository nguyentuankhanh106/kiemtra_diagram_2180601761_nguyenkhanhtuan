﻿using System.ComponentModel.DataAnnotations;

namespace _2180601761_NguyenKhanhTuan_Diagram.Models
{
    public class Logs
    {

        public int Id { get; set; }
        [Required, StringLength(50)]
        public string LoginDate { get; set; }
        public string Logintime { get; set; }

        public int TransactionalId { get; set; }
        public Transactions? Transactions { get; set; }
        public List<Reports> Reports { get; set; }

    }
}
